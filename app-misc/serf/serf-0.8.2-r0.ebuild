# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Serf is a tool for cluster membership, failure detection, and orchestration"
HOMEPAGE="https://www.serf.io/"
SRC_URI="
	arm?  ( https://releases.hashicorp.com/serf/${PV}/serf_${PV}_linux_arm.zip )
	amd64? ( https://releases.hashicorp.com/serf/${PV}/serf_${PV}_linux_amd64.zip )
	"


LICENSE="GPL-2"
SLOT="0"
KEYWORDS="arm amd64"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack(){
	unpack ${A}
	mkdir ${P}
	mv serf ${P}/serf
}

src_install(){
	dobin serf
	newinitd ${FILESDIR}/${P}.initd ${PN} || die
	newconfd ${FILESDIR}/${P}.confd ${PN} || die
}
