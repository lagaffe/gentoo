# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="confd is a lightweight configuration management tool"
HOMEPAGE="http://www.confd.io/"
SRC_URI="https://github.com/kelseyhightower/confd/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="arm arm64 amd64"

DEPEND="dev-lang/go"
RDEPEND="${DEPEND}"
BDEPEND=""

EGO_PN="github.com/kelseyhightower/confd"
inherit golang-build golang-vcs-snapshot

src_compile() {
	pushd src/${EGO_PN} || die
	mkdir -p bin || die
	GOPATH="${S}" go build -o bin/confd . || die "build failed"
	popd || die
}

src_install(){
	pushd src/${EGO_PN} || die
	dobin bin/confd
	popd || die

	newinitd ${FILESDIR}/${P}.initd ${PN} || die
	newconfd ${FILESDIR}/${P}.confd ${PN} || die

	insinto /etc/${PN}/templates
	doins ${FILESDIR}/myapp.conf
	insinto /etc/${PN}/conf.d
}
