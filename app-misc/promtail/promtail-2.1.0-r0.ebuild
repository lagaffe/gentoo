# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Promtail is an agent for a private Loki instance or Grafana Cloud"
HOMEPAGE="https://grafana.com/docs/loki/latest/clients/promtail/"
SRC_URI="
	arm? ( https://github.com/grafana/loki/releases/download/v${PV}/promtail-linux-arm.zip )
	arm64? ( https://github.com/grafana/loki/releases/download/v${PV}/promtail-linux-arm64.zip )
	amd64? ( https://github.com/grafana/loki/releases/download/v${PV}/promtail-linux-amd64.zip )
	"


LICENSE="GPL-2"
SLOT="0"
KEYWORDS="arm arm64 amd64"

IUSE="traefik"

DEPEND="traefik? ( www-servers/traefik )"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack(){
	unpack ${A}
	mkdir ${P}
	mv ${PN}-linux-* ${P}/promtail
}

src_install(){
	dobin promtail
	newconfd "${FILESDIR}/${PN}.confd" ${PN}
	newinitd "${FILESDIR}/${PN}.initd" ${PN}

	if use traefik ; then
		insinto /etc/${PN}
		newins ${FILESDIR}/promtail.yml.proxy promtail.yml
		insinto /etc/traefik/conf.d
		newins ${FILESDIR}/traefik.yml promtail.yml
		einfo "'loki' and 'promtail' entrypoints have to be manually configured in traefik.toml:"
		einfo "  [entrypoints.promtail]"
        einfo "  address = \"192.168.0.1:9080\""
        einfo "  [entrypoints.loki]"
        einfo "  address = \"[::1]:3100\""
		einfo "and of course, restart traefik"
	else
		insinto /etc/${PN}
		newins ${FILESDIR}/promtail.yml.standalone promtail.yml
	fi
}
