# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Prometheus exporter for hardware and OS metrics exposed by *NIX kernels"
HOMEPAGE="https://prometheus.io/"
SRC_URI=" 
    x86? ( https://github.com/prometheus/node_exporter/releases/download/v${PV}/node_exporter-1.1.1.linux-386.tar.gz )
	rpi2? ( https://github.com/prometheus/node_exporter/releases/download/v${PV}/node_exporter-1.1.1.linux-armv6.tar.gz )
	rpi3? ( https://github.com/prometheus/node_exporter/releases/download/v${PV}/node_exporter-1.1.1.linux-armv7.tar.gz )
	arm? ( https://github.com/prometheus/node_exporter/releases/download/v${PV}/node_exporter-1.1.1.linux-armv7.tar.gz )
	arm64? ( https://github.com/prometheus/node_exporter/releases/download/v${PV}/node_exporter-1.1.1.linux-arm64.tar.gz )
	amd64? ( https://github.com/prometheus/node_exporter/releases/download/v${PV}/node_exporter-1.1.1.linux-amd64.tar.gz )
	"


LICENSE="GPL-2"
SLOT="0"
KEYWORDS="arm arm64 amd64"

IUSE="traefik rpi2 rpi3"

DEPEND="traefik? ( www-servers/traefik )"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack(){
	unpack ${A}
	mv ${P}.linux-* ${P}
}


src_install(){
	dobin node_exporter
	newconfd "${FILESDIR}/${PN}.confd" ${PN}
	newinitd "${FILESDIR}/${PN}.initd" ${PN}

	if use traefik ; then
		insinto /etc/${PN}
		insinto /etc/traefik/conf.d
		newins ${FILESDIR}/traefik.yml node_exporter.yml
		einfo "'nodeexporter' entrypoint has to be manually configured in traefik.toml:"
		einfo "  [entrypoints.nodeexporter]"
        einfo "  address = \"10.0.0.239:9100\""
		einfo "and of course, restart traefik"
	fi
}
