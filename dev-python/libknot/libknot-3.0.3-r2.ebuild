# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
PYTHON_COMPAT=( python3_{6..9} pypy3 )

inherit distutils-r1

DESCRIPTION="python library for knot authoritative-only DNS server"
HOMEPAGE="https://www.knot-dns.cz/"
SRC_URI="https://secure.nic.cz/files/knot-dns/knot-${PV}.tar.xz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~alpha amd64 arm arm64 hppa ~ia64 ppc ppc64 ~riscv ~s390 sparc x86 ~x64-macos"

src_unpack(){
	unpack ${A}
	mv knot-${PV} ${P}
}

src_configure() {
	econf
	mv ${WORKDIR}/${P} ${WORKDIR}/knot-${PV}
	mv ${WORKDIR}/knot-${PV}/python ${WORKDIR}/${P}
}

#src_compile() {
#	cd python
	#distutils-r1_python_compile
#}

#src_install() {
#	cd python
#	ls
#	distutils-r1_python_install
#}

