# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Setup network namespace"
HOMEPAGE=""
SRC_URI=""


LICENSE="GPL-2"
SLOT="0"
KEYWORDS="arm arm64 amd64"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack(){
	mkdir ${P}
}

src_install(){
	newconfd "${FILESDIR}/${PN}.confd" ${PN}
	newinitd "${FILESDIR}/${PN}.initd_v01.01.00" ${PN}
	newinitd "${FILESDIR}/kernel_ipv6_forward.initd" kernel_ipv6_forward

	insinto /etc/${PN}
	newins ${FILESDIR}/example.conf namespace1.conf.example

	insinto /etc/sysctl.d
	newins ${FILESDIR}/kernel_ipv6.conf ipv6.conf
}

