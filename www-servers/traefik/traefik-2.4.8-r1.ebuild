# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Traefik is an open-source Edge Router"
HOMEPAGE="https://doc.traefik.io/traefik/"
SRC_URI="https://github.com/traefik/traefik/releases/download/v${PV}/traefik_v${PV}_linux_amd64.tar.gz
	arm? ( https://github.com/traefik/traefik/releases/download/v${PV}/traefik_v${PV}_linux_armv7.tar.gz )
	arm64? ( https://github.com/traefik/traefik/releases/download/v${PV}/traefik_v${PV}_linux_armv7.tar.gz )
	"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="arm arm64 amd64"

DEPEND="acct-user/traefik acct-group/traefik"
RDEPEND="${DEPEND}"
BDEPEND="acct-user/traefik acct-group/traefik"

src_unpack(){
	mkdir ${P}
	cd ${P}
	unpack ${A}
}

src_install(){
	dobin traefik
	newconfd "${FILESDIR}/${PN}.confd" ${PN}
	newinitd "${FILESDIR}/${PN}.initd" ${PN}

	insinto /etc/${PN}
	newins ${FILESDIR}/traefik.toml traefik.toml

	insinto /etc/${PN}/conf.d
	fowners traefik:traefik /etc/${PN}/conf.d
	newins ${FILESDIR}/api.toml api.toml
}
