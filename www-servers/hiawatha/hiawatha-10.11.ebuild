# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="An advanced and secure webserver for Unix"
HOMEPAGE="https://www.hiawatha-webserver.org"
SRC_URI="https://www.hiawatha-webserver.org/files/hiawatha-10.11.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="arm arm64 amd64"

DEPEND="acct-user/hiawatha acct-group/hiawatha"
RDEPEND="${DEPEND}"
BDEPEND="dev-util/cmake acct-user/hiawatha acct-group/hiawatha"

src_prepare(){
	eapply_user
	mkdir build
}

src_configure(){
	cd build
	cmake -DCMAKE_INSTALL_PREFIX="${D}/usr" -DCONFIG_DIR="${D}/etc/hiawatha" -DLOG_DIR="${D}/var/log/hiawatha" -DPID_DIR="${D}/var/run" -DWEBROOT_DIR="${D}/var/www/hiawatha" -DWORK_DIR="${D}/var/lib/hiawatha" ..
}

src_compile(){
	cd build
	if [ -f Makefile ]; then
		emake || die "emake failed"
	else
		die "no Makefile found"
	fi
}

src_install(){
	cd build
	if [[ -f Makefile ]]; then
		emake install/strip
	else
		die "no Makefile found"
	fi

	fowners hiawatha:hiawatha /etc/hiawatha
	fowners hiawatha:hiawatha /var/lib/hiawatha
	fowners hiawatha:hiawatha /var/log/hiawatha
	fowners hiawatha:hiawatha /var/www/hiawatha

	newconfd "${FILESDIR}/${PN}.confd" ${PN}
	newinitd "${FILESDIR}/${PN}.initd" ${PN}
}
