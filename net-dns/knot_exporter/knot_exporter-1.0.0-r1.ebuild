# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Export BIND (named/dns) v9+ service metrics to Prometheus."
HOMEPAGE="https://github.com/salzmdan/knot_exporter"
SRC_URI=""


LICENSE="GPL-2"
SLOT="0"
KEYWORDS="arm arm64 amd64"

IUSE="rpi2 rpi3"

DEPEND="acct-user/knot acct-group/knot dev-python/libknot dev-python/prometheus_client"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack(){
	mkdir ${P}
}

src_install(){
	newbin "${FILESDIR}/${P}" ${PN}
	newconfd "${FILESDIR}/${PN}.confd.v01.00.00" ${PN}
	newinitd "${FILESDIR}/${PN}.initd.v01.00.00" ${PN}
}
