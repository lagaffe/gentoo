# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Export BIND (named/dns) v9+ service metrics to Prometheus."
HOMEPAGE="https://github.com/prometheus-community/bind_exporter"
SRC_URI="
	rpi2? ( https://github.com/prometheus-community/bind_exporter/releases/download/v${PV}/bind_exporter-${PV}.linux-armv6.tar.gz )
	rpi3? ( https://github.com/prometheus-community/bind_exporter/releases/download/v${PV}/bind_exporter-${PV}.linux-armv7.tar.gz )
	arm? ( https://github.com/prometheus-community/bind_exporter/releases/download/v${PV}/bind_exporter-${PV}.linux-armv7.tar.gz )
	amd64? ( https://github.com/prometheus-community/bind_exporter/releases/download/v${PV}/bind_exporter-${PV}.linux-amd64.tar.gz )
	arm64? ( https://github.com/prometheus-community/bind_exporter/releases/download/v${PV}/bind_exporter-${PV}.linux-arm64.tar.gz )
	x86? ( https://github.com/prometheus-community/bind_exporter/releases/download/v${PV}/bind_exporter-${PV}.linux-386.tar.gz )
	"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="arm arm64 amd64"

IUSE="rpi2 rpi3"

DEPEND="acct-user/named acct-group/named"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack(){
	unpack ${A}
	mv ${P}.linux-* ${P}
}

src_install(){
	dobin bind_exporter
	newconfd "${FILESDIR}/${PN}.confd.v01.00.00" ${PN}
	newinitd "${FILESDIR}/${PN}.initd.v01.00.00" ${PN}
}
