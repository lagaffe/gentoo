# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="download urlhaus DNSRPZ zones"
HOMEPAGE=""
SRC_URI=""


LICENSE="GPL-2"
SLOT="0"
KEYWORDS="arm arm64 amd64"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack(){
	mkdir ${P}
}

src_install(){
	dobin ${FILESDIR}/urlhaus
}
