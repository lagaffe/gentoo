# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="grab public key from DNS. Mainly thought for SSHd"
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="arm amd64 x86"
IUSE=""

DEPEND="net-dns/bind-tools"
RDEPEND=${DEPEND}
BDEPEND=""

src_unpack(){
	mkdir ${P}
}

src_install(){
	newsbin "${FILESDIR}/grab_ssh_pubkey.v02.00.00" grab_ssh_pubkey
	newconfd "${FILESDIR}/grab_ssh_pubkey.conf.v02.00.00" grab_ssh_pubkey
	fperms 0750 /usr/sbin/grab_ssh_pubkey
}
