# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="grab public key from DNS. Mainly thought for SSHd"
HOMEPAGE="https://gitlab.com/lagaffe/tools"
SRC_URI="https://gitlab.com/lagaffe/tools/-/archive/0.0.2/tools-0.0.2.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="arm amd64 x86"
IUSE=""

DEPEND="net-dns/bind-tools"
RDEPEND=${DEPEND}
BDEPEND=""

src_unpack(){
	unpack ${A}
	mv tools-* ${P}
}
src_install(){
	dosbin ssh/bin/grab_ssh_pubkey
	doconfd ssh/etc/grab_ssh_pubkey
	fperms 0750 /usr/sbin/grab_ssh_pubkey
}
