# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Stork is an open source management tool for ISC's open source software systems."
HOMEPAGE="https://stork.isc.org/"
SRC_URI="https://ftp.isc.org/isc/stork/0.15.0/stork-0.15.0.tar.gz https://ftp.isc.org/isc/stork/0.15.0/stork-0.15.0.tar.gz.sha512.asc"


LICENSE="GPL-2"
SLOT="0"
KEYWORDS="arm arm64 amd64"

IUSE="agent server"
DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

