# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="install basic tools"

SLOT="0"
KEYWORDS="arm amd64 x86"

DEPEND="app-admin/site-sudo app-admin/sysklogd app-editors/vim app-portage/gentoolkit app-admin/site-chrony net-dns/bind-tools sys-apps/portage sys-devel/distcc sys-process/cronie net-analyzer/fail2ban net-analyzer/tcpdump app-misc/promtail app-misc/node_exporter net-misc/netns net-firewall/nftables app-misc/confd app-misc/serf"
RDEPEND=${DEPEND}
