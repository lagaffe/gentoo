# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="install NAT64 tools"

SLOT="0"
KEYWORDS="arm amd64 x86"

DEPEND="net-proxy/tayga"
RDEPEND=${DEPEND}
