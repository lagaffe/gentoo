# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="install DNS ++ infra"

SLOT="0"
KEYWORDS="arm amd64 x86"

DEPEND="net-dns/unbound net-dns/bind net-dns/knot"
RDEPEND=${DEPEND}
