# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="install wifi tools"

SLOT="0"
KEYWORDS="arm amd64 x86"

DEPEND="app-admin/site-wireless net-wireless/wireless-tools net-wireless/wpa_supplicant"
RDEPEND=${DEPEND}
