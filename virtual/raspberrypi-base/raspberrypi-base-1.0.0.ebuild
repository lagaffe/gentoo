# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="install basic raspberry tools"

SLOT="0"
KEYWORDS="arm"

DEPEND="sys-boot/raspberrypi-firmware sys-kernel/raspberrypi-image "
RDEPEND="${DEPEND}"
BDEPEND=""
