# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="site cups configuration"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="arm arm64 amd64"

DEPEND="app-misc/confd net-print/cups net-print/hplip"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	mkdir ${PF}
}

src_install(){
	insinto /etc/confd/templates
	newins ${FILESDIR}/cupsd.conf-1.0.0 cupsd.conf
	newins ${FILESDIR}/10_cupsd.nft-1.0.0 10_cupsd.nft
	newins ${FILESDIR}/11_cupsd.nft-1.0.0 11_cupsd.nft

	insinto /etc/confd/conf.d
	newins ${FILESDIR}/cupsd.toml-1.0.0 cupsd.toml
	newins ${FILESDIR}/nftables_10_cupsd.toml-1.0.0 nftables_10_cupsd.toml
	newins ${FILESDIR}/nftables_11_cupsd.toml-1.0.0 nftables_11_cupsd.toml
}
