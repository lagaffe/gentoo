# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="site nftables configuration"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="arm arm64 amd64"

DEPEND="app-misc/confd net-firewall/nftables net-proxy/tayga"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	mkdir ${PF}
}

src_install(){
	insinto /etc/confd/templates
	newins ${FILESDIR}/tayga.conf-0.1.0 tayga.conf
	newins ${FILESDIR}/03_nat64.nft-0.1.1 03_nat64.nft

	insinto /etc/confd/conf.d
	newins ${FILESDIR}/tayga.toml-0.1.0 tayga.toml
	newins ${FILESDIR}/nftables_03_nat64.toml-0.1.1 nftables_03_nat64.toml
}
