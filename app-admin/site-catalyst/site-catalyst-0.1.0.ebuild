# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="site catalyst configuration"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64"

DEPEND="app-misc/confd dev-util/catalyst"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	mkdir ${PF}
}

src_install(){
	insinto /etc/confd/templates
	newins ${FILESDIR}/catalyst.conf-1.0.0 catalyst.conf
	insinto /etc/confd/conf.d
	newins ${FILESDIR}/catalyst.toml-1.0.0 catalyst.toml
}
