# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="site sudo configuration"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="arm arm64 amd64"

DEPEND="app-misc/confd net-misc/chrony"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	mkdir ${PF}
}

src_install(){
	insinto /etc/confd/templates
	doins ${FILESDIR}/chrony.conf
	insinto /etc/confd/conf.d
	doins ${FILESDIR}/chrony.toml
}
