# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="site bind (auth slave) configuration"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="arm arm64 amd64"

USE="public intern"

DEPEND="app-misc/confd net-dns/bind"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	mkdir ${PF}
}

src_install(){
	insinto /etc/confd/conf.d
	newins ${FILESDIR}/intern_named_conf.toml-0.1.0 intern_named_conf.toml
	newins ${FILESDIR}/intern_named_init.toml-0.1.0 intern_named_init.toml
	newins ${FILESDIR}/nftables_04_authoritative_intern.toml-0.1.0 nftables_04_authoritative_intern.toml
	newins ${FILESDIR}/nftables_04_authoritative_public.toml-0.1.0 nftables_04_authoritative_public.toml
	newins ${FILESDIR}/public_named_conf.toml-0.1.0 public_named_conf.toml
	newins ${FILESDIR}/public_named_init.toml-0.1.0 public_named_init.toml

	insinto /etc/confd/templates
	newins ${FILESDIR}/04_dns_authoritative.nft-0.1.0 04_dns_authoritative.nft
	newins ${FILESDIR}/named.conf-0.1.1 named.conf
	newins ${FILESDIR}/named.init-0.1.0 named.init
}
