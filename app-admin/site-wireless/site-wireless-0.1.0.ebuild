# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="site wireless configuration"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="arm arm64 amd64"

DEPEND="app-misc/confd net-wireless/wireless-tools net-wireless/wpa_supplicant"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	mkdir ${PF}
}

src_install(){
	insinto /etc/confd/templates
	doins ${FILESDIR}/wpa_supplicant.conf
	insinto /etc/confd/conf.d
	doins ${FILESDIR}/wpa_supplicant.toml
}
