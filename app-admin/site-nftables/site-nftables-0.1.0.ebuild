# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="site nftables configuration"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="arm arm64 amd64"

DEPEND="app-misc/confd net-firewall/nftables"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	mkdir ${PF}
}

src_install(){
	insinto /etc/nftables
	doins ${FILESDIR}/load_and_save.sh

	insinto /etc/confd/templates
	doins ${FILESDIR}/00_base.nft

	insinto /etc/confd/conf.d
	doins ${FILESDIR}/nftables_00_base.toml
}
