#!/bin/sh

BASEDIR='/etc/nftables'

if [ ! $# -eq 1 ]; then
  echo "Usage:"
  echo "  $0 [all|<file>]"
  exit 1
fi

if [ "$1" == "all" ]; then
  for f in `find $BASEDIR -maxdepth 1 -type f -name "[0-9][0-0]_*.nft"`; do
    /sbin/nft -f $f
    res=$?
    if [ ! $res -eq 0 ]; then
      echo "error while applying $f"
      exit $res
    fi
  done
  /etc/init.d/nftables save
  exit $?
fi

if [ ! -f $1 ]; then
  echo "file '$1' does not exist"
  exit 2
fi

/sbin/nft -f $1
res=$?
if [ ! $res -eq 0 ]; then
  echo "error while applying $1"
  exit $res
fi

/etc/init.d/nftables save
exit $?
