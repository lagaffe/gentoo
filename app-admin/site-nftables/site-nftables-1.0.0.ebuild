# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="site nftables configuration"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="arm arm64 amd64"

DEPEND="app-misc/confd net-firewall/nftables"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	mkdir ${PF}
}

src_install(){
	exeinto /etc/nftables
	exeopts -m0744
	doexe ${FILESDIR}/load_and_save.sh

	insinto /etc/confd/templates
	newins ${FILESDIR}/00_base.nft-1.0.0 00_base.nft

	insinto /etc/confd/conf.d
	doins ${FILESDIR}/nftables_00_base.toml
}
