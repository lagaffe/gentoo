# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="site unbount configuration"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="arm arm64 amd64"

DEPEND="app-misc/confd net-dns/unbound"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	mkdir ${PF}
}

src_install(){
	insinto /etc/confd/conf.d
	newins ${FILESDIR}/unbound.toml-0.1.0 unbound.toml
	newins ${FILESDIR}/nftables_02_resolver.toml-0.1.0 nftables_02_resolver.toml

	insinto /etc/confd/templates
	newins ${FILESDIR}/unbound.conf-0.1.0 unbound.conf
	newins ${FILESDIR}/02_dns_resolver.nft-0.1.0 02_dns_resolver.nft
}
