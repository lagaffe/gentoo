# Copyright 2019-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit acct-user

DESCRIPTION="A user for hiawatha"
ACCT_USER_ID=460
ACCT_USER_GROUPS=( hiawatha )

acct-user_add_deps
